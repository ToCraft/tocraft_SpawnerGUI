package pl.tocraft.spawnergui;

import com.sk89q.worldguard.LocalPlayer;
import com.sk89q.worldguard.bukkit.BukkitPlayer;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.managers.RegionManager;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashSet;
import java.util.Set;

public class Main extends JavaPlugin {
    public static final Set<String> openGUIs = new HashSet<>();
    public static Main instance = null;
    private Economy eco = null;
    private WorldGuardPlugin worldguard = null;

    @Override
    public void onDisable() {
        eatGUIs();
    }

    @Override
    public void onEnable() {
        instance = this;

        if (getServer().getPluginManager().getPlugin("Vault") != null) {
            RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
            if (rsp != null) {
                eco = rsp.getProvider();
            }
        }

        if (getServer().getPluginManager().getPlugin("WorldGuard") != null) {
            worldguard = (WorldGuardPlugin) getServer().getPluginManager().getPlugin("WorldGuard");
        }
        getServer().getPluginManager().registerEvents(new Handler(), this);
    }

    public void openGUI(CreatureSpawner spawner, Player p) {
        Spawnable type = Spawnable.from(spawner.getSpawnedType());
        GUIHandler gui = new GUIHandler("Spawner: " + type.getName(), 36, spawner);
        int j = 0;

        for (Spawnable e : Spawnable.values()) {
            int price = e.getCost();
            String priceLine = price > 0 ? "§e" + price : "§aDarmowy";

            priceLine += (p.hasPermission("spawnergui.eco.bypass." + e.getName().toLowerCase()) || p.hasPermission("spawnergui.eco.bypass.*")) && price > 0.0 ? " §a§o(Free for you)" : "";

            if (eco != null) {
                gui.setItem(j, e.getSpawnEgg(), "§6" + e.getName(), "§7Cena: " + priceLine);
            } else {
                gui.setItem(j, e.getSpawnEgg(), "§6" + e.getName());
            }
            j++;
        }

        gui.open(p);
        openGUIs.add(p.getName());
    }

    public void eatGUIs() {
        Bukkit.getOnlinePlayers().stream().filter(p -> openGUIs.contains(p.getName())).forEach(p -> {
            p.getOpenInventory().close();
            p.sendMessage("§cInterfejs zostal przymusowo zamkniety w celu unikniecia bugow.");
        });
    }

    private boolean canOpenAtLoc(Player p, Location loc) {
        if (worldguard != null && !p.isOp()) {
            RegionManager r = worldguard.getRegionManager(loc.getWorld());

            if (r != null) {
                ApplicableRegionSet regions = r.getApplicableRegions(loc);
                LocalPlayer lp = new BukkitPlayer(worldguard, p);
                return regions.canBuild(lp);
            }
        }
        return true;
    }

    public class Handler implements Listener {
        @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
        public void handleInteract(PlayerInteractEvent event) {
            if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                Block b = event.getClickedBlock();
                Player p = event.getPlayer();

                if (b != null && b.getType() == Material.MOB_SPAWNER) {
                    event.setCancelled(true);

                    if (!canOpenAtLoc(p, b.getLocation())) {
                        p.sendMessage("§cTen spawner jest zabezpieczony.");
                        return;
                    }
                    openGUI((CreatureSpawner) b.getState(), p);
                }
            }
        }

        @EventHandler
        public void handleClick(GUIClickEvent event) {
            Player p = event.getPlayer();
            CreatureSpawner spawner = event.getSpawner();

            if (spawner.getBlock().getType() != Material.MOB_SPAWNER) {
                p.sendMessage("§cThe spawner block is no longer valid! (§7" + spawner.getBlock().getType().name().toLowerCase() + "§c)");
                return;
            }
            String clicked = ChatColor.stripColor(event.getItem().getItemMeta().getDisplayName().toLowerCase());
            Spawnable current = Spawnable.from(spawner.getSpawnedType());

            if (clicked.equalsIgnoreCase("balance")) {
                event.setWillClose(false);
            } else {
                for (Spawnable e : Spawnable.values()) {
                    if (clicked.equalsIgnoreCase(e.getName().toLowerCase())) {
                        if (eco != null && !p.hasPermission("spawnergui.eco.bypass.*")) {
                            double price = p.hasPermission("spawnergui.eco.bypass." + clicked) ? 0.0 : e.getCost();

                            if (price > 0.0) {
                                if (eco.has(p, price)) {
                                    p.sendMessage("§7Pobrano §f" + price + " §7z twojego konta.");
                                    eco.withdrawPlayer(p, price);
                                } else {
                                    p.sendMessage("§cAby zmienic spawner musisz miec §7" + price);
                                    return;
                                }
                            }
                        }
                        spawner.setSpawnedType(e.getType());
                        spawner.update(true);
                        assert current != null;
                        p.sendMessage("§9Typ spawnera zmieniony z §7" + current.getName().toLowerCase() + " §9na §7" + clicked + "§9!");
                        return;
                    }
                }
            }
        }
    }
}

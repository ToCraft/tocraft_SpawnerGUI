package pl.tocraft.spawnergui;

import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.inventory.ItemStack;

public enum Spawnable {
    CREEPER(EntityType.CREEPER, "Creeper", 25000, (byte) 50),
    SKELETON(EntityType.SKELETON, "Szkielet", 15000, (byte) 51),
    SPIDER(EntityType.SPIDER, "Pajak", 13000, (byte) 52),
    ZOMBIE(EntityType.ZOMBIE, "Zombie", 17000, (byte) 54),
    SLIME(EntityType.SLIME, "Slime", 35000, (byte) 55),
    PIG_ZOMBIE(EntityType.PIG_ZOMBIE, "Swinia zombie", 9000, (byte) 57),
    ENDERMAN(EntityType.ENDERMAN, "Enderman", 45000, (byte) 58),
    CAVE_SPIDER(EntityType.CAVE_SPIDER, "Pajak jaskiniowy", 40000, (byte) 59),
    BLAZE(EntityType.BLAZE, "Blaze", 100000, (byte) 61),
    MAGMA_CUBE(EntityType.MAGMA_CUBE, "Kostka magmy", 10000, (byte) 62),
    WITCH(EntityType.WITCH, "Wiedzma", 75000, (byte) 66),
    PIG(EntityType.PIG, "Swinia", 5000, (byte) 90),
    SHEEP(EntityType.SHEEP, "Owca", 7500, (byte) 91),
    COW(EntityType.COW, "Krowa", 15000, (byte) 92),
    CHICKEN(EntityType.CHICKEN, "Kurczak", 12000, (byte) 93),
    IRON_GOLEM(EntityType.IRON_GOLEM, "Zelazny golem", 200000, (byte) 99),
    HORSE(EntityType.HORSE, "Kon", 15000, (byte) 100),
    VILLAGER(EntityType.VILLAGER, "Osadnik", 25000, (byte) 120);

    private final EntityType type;
    private final String name;
    private final byte data;
    private final int cost;

    Spawnable(EntityType type, String name, int cost, byte data) {
        this.type = type;
        this.name = name;
        this.data = data;
        this.cost = cost;
    }

    public static Spawnable from(EntityType type) {
        for (Spawnable s : values()) {
            if (s.getType() == type) {
                return s;
            }
        }
        return null;
    }

    public String getName() {
        return name;
    }

    public byte getData() {
        return data;
    }

    public EntityType getType() {
        return type;
    }

    public int getCost() {
        return cost;
    }

    public ItemStack getSpawnEgg() {
        return new ItemStack(Material.MONSTER_EGG, 1, data);
    }
}
